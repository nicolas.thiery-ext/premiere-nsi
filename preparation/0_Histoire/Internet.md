# Brève histoire de l'Internet

## L'ancêtre

On peut noter la présence d'un premier réseau dès les années 1960, financé
par le [DoD](https://duckduckgo.com/!wikipedia%20DoD) :
[ArpaNet](https://duckduckgo.com/!wikipedia%20Arpanet). Ce réseau reliait au
départ que quelques sites, puis a été étendu
