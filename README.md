# Présentation du dépot

Ce dépot contient des cours et documents élaborés dans le cadre de
l'enseignement de [spécialité Numérique et Sciences
Informatiques](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)
de la classe de [Première](https://eduscol.education.fr/92/j-enseigne-au-lycee-generaltechnologique) pendant les années 2019/2020 (première année de l'enseignement) et 2020/2021.

Le dépôt est organisé en plusieurs parties distinctes :
+ [LaTeX](./LaTeX) contient des sources de documments au format $\LaTeX$.
+ [@src](./@src) contient des scripts [Python](https://python.org) pour
générer et distribuer les documents PDF sur plusieurs plateformes.
+ [jupyter](./jupyter) contient des fichiers sources au format .ipynb pour
générer un site de cours.
+ [outils](./outils) contient des modules permettant de masquer certains
points de Python pour mettre en avant d'autres points.
+ [.gitlab-ci.yml](./.gitlab-ci.yml) contient les informations nécessaires à
la génération des fichiers en utilisant les fonctionnalités d'[intégration
continue/déploiement
continu](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/)
+ [requirements.txt] contient une liste de modules Python nécessaires.

Pour avoir un environnement de développement similaire, je conseille
d'installer localement sur sa machine (testé sous
[GNU/Linux](http://www.gnu.org/gnu/why-gnu-linux.html) les logiciels
suivants :
+ neovim avec les plugins :
  + UtilSnip
  + vimtex
  + jupytext
  + iPy-vim
  + fugitive
  + gitlab-fugitive
+ vscodium avec les extensions :
  + ExcutableBook-Myst
  + TeX
  + Python
+ jupyter
+ jupytext
+ gitlab-runner
+ pipenv (controversé)
+ texlive
+ latexmk
+ firefox
