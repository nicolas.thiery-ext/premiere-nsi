# Sur les exercices

La partie suivante regroupe les exercices du cours de NSI de première au lycée. Ils sont à chercher dans le cahier et leur correction est à noter. Une partie de ces exercices peut-être réutilisée pour les devoirs.

Les exercices à effectuer sur ordinateur :computer: seront à effectuer sur le site [](outils/capytale).
