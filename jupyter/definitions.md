---
orphan: true
---
# Définitions

IHM
: Interface Homme Machine, désigne tout ce qui permet la communication, dans
un sens ou dans l'autre entre un être humain et une machine.
