# Exercices sur les variables

:::{admonition} Exercice 1
:class: exercice

1. Répondre aux questions suivantes
    1. Comment nomme-t-on une variable permettant de stocker des valeurs logiques ?
    2. Si on a tapé `a = 5`, quel est le type de `a` ?
2. Que produit l'appel `7 = b` ?

:::

::::{admonition} Exercice 2
:class: exercice

On entre les valeurs suivantes sur la console :
:::{code-block} pycon
>>> a,b,c = 4,2,1
:::

Représenter ce qui se passe par un schéma avec des flèches

::::

::::{admonition} Exercice 3
:class: exercice

Écrire le tableau d'affectation des variables dans les cas suivants.

1. 
    :::{code-block} python
    a = 2
    b = 3
    c = a ** b
    d = c % b
    :::
2. 
    :::{code-block} python
    a = 3
    b = 2
    c = (b * a) ** b
    d = (c + b) // a
    :::
3.
    :::{code-block} python
    a = 5
    b = 2
    c=a/b
    :::
4.
    :::{code-block} python
    a = 5
    b = 2
    c=a/b
    :::
5.
    :::{code-block} python
    a = 5
    b = 2
    c=a//b
    :::
6. 
    :::{code-block} python
    a = 5
    b = 2
    c=a%b
    :::
7. 
    :::{code-block} python
    a = 6
    b = 2
    c=a/b =
    :::
::::

::::{admonition} Exercice 4
:class: exercice

1. Donner le type de la variable `rep` si `rep = x**2 + y** 2 == z**2`
2. S'agit-il d'une instruction ou d'une évaluation ?
3. 
    1. Que vaut `rep` si `x = 5`, `y = 5` et `z = 2` ?
    1. Que vaut `rep` si `x = 6`, `y = 8` et `z = 10` ?

::::

::::{admonition} Exercice 5
:class: exercice

Donner le type et le résultat de l'expression `var1 + var2` si c'est possible. Indiquer si le résultat n'est pas possible.

1.
    :::{code-block} python
    var1=4
    var2=7
    :::
1.
    :::{code-block} python
    var1="4"
    var2=7
    :::
1.
    :::{code-block} python
    var1="4"
    var2="7"
    :::
::::


---

::::{admonition} Exercice 6
:class: exercice

1. Que contient la variable `cpte` à la fin de l’exécution de ce script ?

    :::{code-block} python
    cpte = 10
    cpte = cpte + cpte
    cpte = cpte + cpte
    :::
2. Que contient la variable `variable2` à la fin de l’exécution de ce script ?

    :::{code-block} python
    variable1 = 5
    variable2 = 2
    variable1 = variable2
    variable1 = variable2 + variable1
    :::
::::

::::{admonition} Exercice 7
:class: exercice

Donner le type de `var` dans les cas suivants :

1. `var=45+50`
2. `var="45<50"`
3. `var=45+50+30.0`
4. `var=45<50`

::::

::::{admonition} Exercice 8
:class: exercice

Quelle est la syntaxe correcte pour affecter la valeur 5 à une variable nommée `age` en Python ?

::::


::::{admonition} Exercice 9
:class: exercice

Si `x=3` et que l'instruction suivante est exécutée :

:::{code-block} pycon
>>> x = x + 2
:::

Quelle sera la nouvelle valeur de `x` ? 

::::
