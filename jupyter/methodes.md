# Conseils pour l'année

## Matériel pour l'année

Pour travailler en NSI en classe et à la maison, vous avez besoin de matériel :

+ un **cahier** pour noter les points de cours et vos idées pendant les projets et les T.D.
+ une clef USB pour sauvegarder votre travail et l'échanger facilement avec vos camarades ou l'enseignant
+ l'ordinateur fourni par la région Île de France, avec les **informations d'identification**

L'essentiel du travail en T.D. sera mené sur [Capytale](outils/capytale), un outil permettant de créer des feuilles de calcul interactive.

Pour les projets, nous utiliserons différents outils qui seront présentés au fur et à mesure. Au lycée, nous utilisons Python 3.10. Vous pouvez [télécharger la dernière version](https://www.python.org/downloads/) et l'installer chez vous.
Pour rédiger les programmes et les documents, nous utilisons VSCodium. Vous pouvez télécharger une [version respectueuse de votre vie privée](https://vscodium.com/#install)

## Méthodes de travail en NSI

L'enseignement «Numérique et Science Informatique» est un enseignement de voie générale avec une partie de pratique. Les séances en classe se déclinent dans les temps suivants :
- activité de découverte et d'expérimentation ;
- cours théoriques permettant de donner un cadre aux notions ;
- travaux dirigés permettant de mettre en pratique les notions ;
- projet transversaux permettant de réutiliser les notions.
Chacun de ses éléments participe à la construction du savoir informatique et on ne peut faire l'économie.

Il est donc essentiel de 
1. noter le cours dans un cahier ;
2. effectuer les exercices types, en particulier en activité débranchée ;
3. réaliser les T.D. sans attendre la correction ;
4. participer raisonnablement aux projets.

## Évaluations

Les évaluations peuvent prendre différents modes :
- évaluations flash
- QCM
  Dans ce cas, une bonne réponse rapporte 3 points, une mauvaise réponse retire 1 point, et l'absence de réponse n'apporte ni ne retire aucun point. Les notes sont capées à 5.
- devoirs sur table
- travaux dirigés en classe
- projets
- exposés
